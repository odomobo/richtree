<!--
Title: Main
Sort: alpha
-->
Your [RichTree][] installation is successful! To get started, take a look at the [markdown][] format,
and read the "How to Use RichTree" section in the [RichTree][] project description.

[RichTree]: http://www.joshodom.net/richtree/
[markdown]: http://daringfireball.net/projects/markdown/syntax