<?php
include_once('config.php');
include_once('lib/richtree.php');

// richtree is used to find the relative uri
$rt = new richtree();

// Set up variables

$sleepTimeBetweenTries = 10;
$numTries = 30; // 5 minutes' worth of retries, to obtain the image resizing lock
$lockFile = 'cache/thumbnailer.lock';
$staleLockTime = 60; // 1 minute

// find the file, by getting the relative URL, and prepending "content" to that
$tmpFile = urldecode($_GET['file']);
$relFile = $rt->get_relative_uri($tmpFile);
$strFile = 'content/'. $relFile;

$intWidth = isset($_GET['width']) ? intval($_GET['width']) : THUMBNAIL_WIDTH;
$intHeight = isset($_GET['height']) ? intval($_GET['height']) : THUMBNAIL_HEIGHT;

// this function will be useful
function do_404() {
    header("HTTP/1.1 404 Not Found");
    exit();
}

// Do a little sanity checking. If the dimensions aren't ints, or both the dimensions are too big, then we don't want to process it.
if (!$intWidth or $intWidth <= 0 or !$intHeight or $intHeight <= 0 or ($intWidth > 500 and $intHeight > 500) )
    do_404();

// Stat the file to see if it exists, and get its properties.
$stat = @stat($strFile);
if (!$stat)
    do_404();

// We're going to make the entity tag from information about the file's last-modified time and size.
// Theoretically, if the file changes, so will one of these.
$strMd5 = md5($stat['mtime'] .";". $stat['size']);
$ETag = '"'. $strMd5 .'"';
$lastModified = gmdate('D, d M Y H:i:s', $stat['mtime']) .' GMT';

// If the browser already knows about this entity tag, our work is done! On a 304 response, it'll simply display the image from its cache.
if ($_SERVER['HTTP_IF_NONE_MATCH'] == $ETag || $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $lastModified) {
    header("HTTP/1.1 304 Not Modified");
    header("ETag: $ETag");
    header("Last-Modified: $lastModified");
    exit();
}

// Create a unique filename based on the source file, and the requested size. This will be the cache file for the thumbnail.
// If this exists, then we can just serve it up.
$strCacheFile = 'cache/' . $strMd5 .'_'. $intWidth .'x'. $intHeight .'.jpeg';
$claimedLock = false;
if (file_exists($strCacheFile)) {
    header("ETag: $ETag");
    header("Last-Modified: $lastModified");
    header("Content-type: image/jpeg");
    readfile($strCacheFile);
    exit();
}

// If the image didn't exist in cache, let's try to create it. First, we get information about the file.
// If we can't, we assume that it's not really an image.
$imageSize = @getimagesize($strFile);
if ($imageSize == false or ($imageSize[2] != IMAGETYPE_PNG and $imageSize[2] != IMAGETYPE_GIF and $imageSize[2] != IMAGETYPE_JPEG)) {
    do_404();
}



$sourceAspectRatio = $imageSize[0] / $imageSize[1];
$requestAspectRatio = $intWidth / $intHeight;

// Correct the destination aspect ratio, by shrinking the larger dimension.
if ($requestAspectRatio > $sourceAspectRatio) {
    // width (numerator) is too great on request aspect ratio
    $intWidth = intval($sourceAspectRatio * $intHeight);
} else {
    // height (denominator) is too great
    $intHeight = intval((1/$sourceAspectRatio) * $intWidth);
}

// We don't want to upscale the thumbnail...
if ($intWidth >= $imageSize[0]) {
    switch($imageSize[2]) {
        case IMAGETYPE_PNG:
            $type = "image/png"; break;
        case IMAGETYPE_GIF:
            $type = "image/gif"; break;
        case IMAGETYPE_JPEG:
            $type = "image/jpeg"; break;
    }
    header("ETag: $ETag");
    header("Last-Modified: $lastModified");
    header("Content-type: $type");
    readfile($strFile);
    exit();
}

for ($i = 0; $i < $numTries; $i++) {
    // if there's no lock file, then we are allowed to create the thumbnail
    $lockStat = @stat($lockFile);
    if ($lockStat === false) {
        touch($lockFile);
        $claimedLock = true;
        break;
    }

    // if the lock file is stale, then claim it as our own
    if (time() - $lockStat['mtime'] > $staleLockTime) {
        touch($lockFile);
        $claimedLock = true;
        break;
    }

    sleep($sleepTimeBetweenTries);
    clearstatcache();

    // we loop over this, because if there's a lock, there's a chance the other process is trying to serve up this file
    if (file_exists($strCacheFile)) {
        header("ETag: $ETag");
        header("Last-Modified: $lastModified");
        header("Content-type: image/jpeg");
        readfile($strCacheFile);
        exit();
    }
}

if (!$claimedLock) {
    header("HTTP/1.1 408 Request Timeout");
    exit();
}

// create the gd image using the correct function
switch($imageSize[2]) {
    case IMAGETYPE_PNG:
        $im = imagecreatefrompng($strFile); break;
    case IMAGETYPE_GIF:
        $im = imagecreatefromgif($strFile); break;
    case IMAGETYPE_JPEG:
        $im = imagecreatefromjpeg($strFile); break;
}

$imResized = imagecreatetruecolor($intWidth, $intHeight);

// Save the file to a temporary file in the cache, and move it after it's created.
// This is because creating the file isn't necessarily an atomic operation, but moving it is.
$tmpStrCacheFile = $strCacheFile .'.tmp';

// these 2 lines do 99% of the work
imagecopyresampled($imResized, $im, 0, 0, 0, 0, $intWidth, $intHeight, $imageSize[0], $imageSize[1]);
imagejpeg($imResized, $tmpStrCacheFile, 85);

@rename($tmpStrCacheFile, $strCacheFile);
@unlink($lockFile);

header("ETag: $ETag");
header("Last-Modified: $lastModified");
header("Content-type: image/jpeg");
readfile($strCacheFile);
exit();
