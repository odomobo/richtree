<?php

class richtree {
    const DEFAULT_TEMPLATE = 'default.html';
    static $HIDDEN_FILES = array('index.md', 'Thumbs.db', '@eaDir');

    function __construct() {
        $this->uri = $this->clean_uri( urldecode($_SERVER['REQUEST_URI']) );
        if (defined('ROOT_URI'))
            $this->root_uri = $this->clean_uri('/'. ROOT_URI .'/');
        else
            $this->root_uri = $this->clean_uri( urldecode( dirname( $_SERVER['PHP_SELF']) ) .'/' );

        $this->assets = $this->root_uri .'themes/'. THEME .'/assets';
    }

    function get_template($uri = null) {
        is_null($uri) && $uri = $this->uri;
        $info = $this->get_node_info($uri);
        $template = isset($info['template']) ? $info['template'] : self::DEFAULT_TEMPLATE;
        return $template;
    }


    /**
     * The following methods are designed to be called directly by the template. They provide data
     * in a concise format. Note that almost all methods which accept URI, default it to the current
     * URI.
     */

    function bots_can_index($uri = null) {
        is_null($uri) && $uri = $this->uri;
        $info = $this->get_node_info($uri);
        if (isset($info['noindex'])) {
            return false;
        }

        if ($uri == $this->root_uri) {
            return true;
        } else {
            return $this->bots_can_index($this->get_parent($uri));
        }
    }

    function can_comment($uri = null) {
        is_null($uri) && $uri = $this->uri;
        $info = $this->get_node_info($uri);
        if ($this->DISQUS_SHORTNAME && isset($info['comments'])) {
            return true;
        } else {
            return false;
        }
    }

    function get_body($uri = null) {
        is_null($uri) && $uri = $this->uri;
        static $cache = array();
        if (isset($cache[$uri]))
            return $cache[$uri];

        $filepath = $this->get_filepath($uri);
        if (is_readable($filepath)) {
            $raw_body = file_get_contents($filepath);
            $p = new Parsedown();
            $body = $p->text($raw_body);
        } else {
            $body = '';
        }

        $cache[$uri] = $body;
        return $body;
    }

    function get_breadcrumbs($uri = null) {
        is_null($uri) && $uri = $this->uri;
        static $cache = array();
        if (isset($cache[$uri]))
            return $cache[$uri];

        $breadcrumbs = array();
        $tmp_uri = $uri;
        while($tmp_uri != $this->root_uri) {
            array_unshift($breadcrumbs, $tmp_uri);
            $tmp_uri = $this->get_parent($tmp_uri);
        }
        if (count($breadcrumbs) <= 1)
            $breadcrumbs = array();
        $cache[$uri] = $breadcrumbs;
        return $breadcrumbs;
    }

    function get_children($uri = null) {
        is_null($uri) && $uri = $this->uri;
        $tmp = $this->get_node_links($uri);
        return $tmp['nodes'];
    }

    function get_copyright_year($timestamp = null) {
        if ($timestamp === null)
            $timestamp = time();
        return date('Y', $timestamp);
    }

    function get_description($uri = null) {
        is_null($uri) && $uri = $this->uri;
        $info = $this->get_node_info($uri);
        if (isset($info['description'])) {
            return $info['description'];
        } else {
            return "";
        }
    }

    function get_files($uri = null) {
        is_null($uri) && $uri = $this->uri;
        $tmp = $this->get_node_links($uri);
        return $tmp['files'];
    }

    function get_image_description($image) {
        $lower = strtolower($image);
        $info = $this->get_node_info($this->uri);
        if (isset($info[$lower])) {
            return $info[$lower];
        } else {
            return $image;
        }
    }

    function get_image_files($uri = null) {
        is_null($uri) && $uri = $this->uri;
        $files = $this->get_files($uri);
        $ret = array();
        foreach ($files as $file) {
            if (preg_match('/\.(png|jpe?g|gif)$/', $file['name']))
                $ret[] = $file;
        }
        return $ret;
    }

    function get_image_thumb($image) {
        // if relative image path, we need to turn it into an absolute path
        if ($image[0] != '/') {
            // we add an arbitrary character because we want dirname("/example/") to return "/example/", not "/"
            $image = dirname( urldecode($_SERVER['REQUEST_URI']) .'x') .'/'. $image;
        }

        return $this->root_uri .'thumbnailer.php?file='. rawurlencode($image) .'&width=356&height='. THUMBNAIL_HEIGHT;
    }

    function get_menu() {
        $tmp = $this->get_children($this->root_uri);
        array_unshift($tmp, $this->root_uri);
        return $tmp;
    }

    function get_parent($uri) {
        if ($uri == $this->root_uri) {
            trigger_error('Cannot get parent of root URI: '. $uri);
            return null;
        }
        return $this->clean_uri(dirname($uri) .'/');
    }

    function get_sitename() {
        return $_SERVER['SERVER_NAME'];
    }

    function get_title($uri = null) {
        is_null($uri) && $uri = $this->uri;
        $info = $this->get_node_info($uri);

        if (isset($info['title'])) {
            $title = $info['title'];
        } else {
            $title = ucfirst(basename($uri));
            $title = preg_replace('/[-_]/', ' ', $title);
            if ($title == '')
                $title = 'Home';
        }
        return $title;
    }

    function is_child($child_uri, $parent_uri) {
        return strpos($child_uri, $parent_uri) === 0;
    }

    function is_file_regular_file($file) {
        return !!($file['stat']['mode'] & 0100000);
    }

    function pretty_date($timestamp) {
        if ($timestamp === null)
            $timestamp = time();
        return date("M d, Y", $timestamp);
    }

    function pretty_filesize($filesize) {
        $suffixes = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($filesize) - 1) / 3);

        // filesize may have started off as an int, but by golly, it'll become a string one way or another!
        if (strlen($filesize) > 3) {
            $filesize = substr($filesize, 0, -($factor*3));
        }
        if ($factor == 0) {
            $filesize = "1";
            $factor = 1;
        }
        return array(
            'filesize' => $filesize,
            'suffix' => @$suffixes[$factor]
        );
    }

    function __get($name) {
        // only provide automagic lookup for constants, which have to be in upper-case
        if ($name != strtoupper($name)) {
            $trace = debug_backtrace();
            trigger_error(
                'Undefined property via __get(): ' . $name .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'],
                E_USER_NOTICE);
            return null;
        }

        // if the constant is set, use that
        if (defined($name) && constant($name)) {
            $val = constant($name);
        } else if (defined('self::DEFAULT_'. $name)) {
            $val = constant('self::DEFAULT_'. $name);
        } else {
            $val = "";
        }

        $this->{$name} = $val;
        return $val;
    }

    function __isset($name) {
        // if it's all in uppercase, then we can retrieve the value
        return $name == strtoupper($name);
    }


    /**
     * The following methods are the real workhorses of richtree. They get information about a given URI, based on
     * the filesystem.
     */

    function get_node_links($uri = null) {
        is_null($uri) && $uri = $this->uri;
        static $cache = array();
        if (isset($cache[$uri]))
            return $cache[$uri];

        $ret = array(
            'files' => array(),
            'nodes' => array()
        );

        // return no links if uri doesn't contain trailing slash (i.e. URI is a file, not a directory)
        if (substr($uri, -1) != '/') {
            $cache[$uri] = $ret;
            return $ret;
        }

        $info = $this->get_node_info($uri);
        if ($info === false) {
            trigger_error('Cannot get links for uri: '. $uri);
            return false;
        }
        if (isset($info['hide'])) {
            $hide = explode(',', $info['hide']);
            foreach ($hide as &$hidden) {
                $hidden = trim($hidden);
            }
        } else {
            $hide = array();
        }

        $uri_path = 'content'. $this->get_relative_uri($uri);

        $results = scandir($uri_path);
        foreach ($results as $result) {
            // don't show hidden files (prepended with a dot), explicitly hidden files, or the index.md file
            if ($result[0] == '.' || in_array($result, $hide) || in_array($result, self::$HIDDEN_FILES))
                continue;

            // Stat the file, so it only has to be checked once. This is a relatively-expensive operation
            $stat = stat($uri_path .'/'. $result);
            if ($stat === false)
                continue;

            if ($stat['mode'] & 040000) { // is directory?
                $result_uri = $uri . $result .'/';
                // We don't even test if it's a valid URI. Either it's a richtree directory (and thus
                // we include it), or else it's another directory... and thus we include it.
                // However, if it's set as "noindex", then we don't want to list it.
                $tmp_info = $this->get_node_info($result_uri);
                if (isset($tmp_info['noindex']))
                    continue;

                $ret['nodes'][] = $result_uri;
            } else if ($stat['mode'] & 0100000) { // is file?
                if (preg_match('/^(.*)\.md$/', $result, $matches)) {
                    $result_uri = $uri . $matches[1];
                    $ret['nodes'][] = $result_uri;
                } else {
                    $ret['files'][] = array('name' => $result, 'stat' => $stat);
                }
            }
        }
        if (!isset($info['sort']) || $info['sort'] == 'date_desc') {
            usort($ret['nodes'], "_compare_nodes_mtime_desc");
        } else if ($info['sort'] == 'alpha') {
            // already sorted by alpha
        } else if ($info['sort'] == 'weight_desc' || $info['sort'] == 'weight') {
            usort($ret['nodes'], "_compare_nodes_weight_desc");
        } else {
            trigger_error('Bad sorting argument for uri "'. $uri .'": '. $info['sort']);
        }

        $cache[$uri] = $ret;
        return $ret;
    }

    function get_node_info($uri = null) {
        is_null($uri) && $uri = $this->uri;
        static $cache = array();
        if (isset($cache[$uri]))
            return $cache[$uri];

        $info = array();
        $filepath = $this->get_filepath($uri);
        $stat = @stat($filepath);
        if ($stat !== false && ($stat['mode'] & 0100000)) {
            $fp = fopen($filepath, 'r');
            $line = fgets($fp);

            // We can't extract info from the file if it doesn't start with a proper comment
            if (!preg_match('/^[\s]*<!--[\s]*$/', $line))
                goto close;

            while(!feof($fp)) {
                $line = fgets($fp);
                if (preg_match('/-->/', $line))
                    break;

                // This is the real workhorse. If a line matches the "Option: value" format, then we store it
                if (preg_match('/^[\s]*([\w]+)[\s]*:(.*)$/', $line, $matches)) {
                    $option = strtolower($matches[1]);
                    $value = trim($matches[2]);
                    $info[$option] = $value;
                }
            }

            close:
            fclose($fp);

        // not a file? let's see if it's a plain directory, with no .md
        } else {
            $filepath = $this->get_relative_uri($uri);
            $stat = @stat('content'. $filepath);
            if (substr($uri, -1) != "/" || $stat === false || !($stat['mode'] & 040000)) { // if it's not a directory, then it's really a bad URI.
                trigger_error('Invalid uri: '. $uri);
                $cache[$uri] = false;
                return false;
            }
        }

        $info['mtime'] = $stat['mtime'];
        $cache[$uri] = $info;
        return $info;
    }

    /**
     * The following methods are internal helper methods, for richtree.
     */

    // the filepath needs to be based on the relative URI
    function get_filepath($uri = null) {
        is_null($uri) && $uri = $this->uri;

        $relative_uri = $this->get_relative_uri($uri);

        // if the URI is a directory, the filepath is going to be index.md
        if (substr($relative_uri, -1) == '/') {
            return 'content'. $relative_uri .'index.md'; // uri contains beginning and ending slashes

        // if the URI is not a directory, the filepath is going to just end with .md
        } else {
            return 'content'. $relative_uri .'.md'; // uri contains beginning slash
        }
    }

    // Remove double slashes from all URIs
    function clean_uri($uri) {
        $uri = preg_replace('.//+.', '/', $uri);
        return $uri;
    }

    // change from /root/whatever to /whatever
    function get_relative_uri($uri = null) {
        is_null($uri) && $uri = $this->uri;

        // change from /root/ to /root
        $tmp_root_uri = substr($this->root_uri, 0, -1);
        // check that root_uri is a substring of the request uri. If not, the root URI has to be configured manually
        if ($tmp_root_uri != "" && strpos($uri, $tmp_root_uri) !== 0) {
            if (!defined('ROOT_URI'))
                trigger_error('ROOT_URI must be set manually in config.php', E_USER_ERROR);
            else
                trigger_error('ROOT_URI is set to a bad value in config.php', E_USER_ERROR);
        }

        $relative_uri = substr_replace($uri, '', 0, strlen($tmp_root_uri));
        return $relative_uri;
    }
}

// for sorting
function _compare_nodes_mtime_desc($r1, $r2) {
    global $rt;
    $r1_info = $rt->get_node_info($r1);
    $r2_info = $rt->get_node_info($r2);
    return $r2_info['mtime'] - $r1_info['mtime'];
}

function _compare_nodes_weight_desc($r1, $r2) {
    global $rt;
    $r1_info = $rt->get_node_info($r1);
    $r2_info = $rt->get_node_info($r2);

    if (isset($r1_info['weight']) && isset($r2_info['weight'])) {
        $diff = floatval($r2_info['weight']) - floatval($r1_info['weight']);
        if ($diff > 0) {
            return 1;
        } else if ($diff < 0) {
            return -1;
        } else {
            return 0;
        }
    } else if (isset($r1_info['weight'])) {
        return -1; // a negative number means $r1 comes first
    } else if (isset($r2_info['weight'])) {
        return 1; // a positive number means $r2 comes first
    } else { // fall back on sorting alphabetically
        return strcmp($rt->get_title($r1), $rt->get_title($r2));
    }
}
