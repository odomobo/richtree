<?php
define('THEME', 'richtree-2015');
define('THUMBNAIL_WIDTH', 200);
define('THUMBNAIL_HEIGHT', 200);

// Optional
//define('COPYRIGHT_OWNER', "Your Name Here");
//define('DISQUS_SHORTNAME', 'disqus shortname here');
//define('GA_TAG', 'ga tag here');
//define('ROOT_URI', '/root/uri/here/');
