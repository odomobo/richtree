<?php
include_once('config.php');
include_once('lib/richtree.php');
include_once('lib/Parsedown.php');

$rt = new richtree();

include_once('lib/Twig/Autoloader.php');
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('themes/'. THEME .'/templates');
$twig = new Twig_Environment($loader, array('cache' => 'cache', 'auto_reload' => true));

// If it's an invalid URI, first check to see if it's an issue with the trailing slash.
// Next, check if it's actually a theme asset.
// Finally, show a 404 page.
if (@$rt->get_node_info() === false) {
    // if they provided a directory, check to see if they meant to provide a file
    if (substr($rt->uri, -1) == "/") {
        $tmp_uri = substr($rt->uri, 0, -1);
        if (@$rt->get_node_info($tmp_uri) !== false) {
            // do redirect
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: '. $tmp_uri);
            exit();
        }

    // if they provided a file, check to see if it should have been a directory
    } else {
        $tmp_uri = $rt->uri .'/';
        if (@$rt->get_node_info($tmp_uri) !== false) {
            // do redirect
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: '. $tmp_uri);
            exit();
        }
    }

    // if the request is for a theme asset, then load that
    $relative_uri = $rt->get_relative_uri();
    $filepath = 'themes/'. THEME . $relative_uri;
    $stat = @stat($filepath);
    // if it's a regular file in the theme directory, then let's deliver the content
    if ($stat !== false && $stat['mode']&0100000) {
        header('Location: '. $rt->root_uri . $filepath);
        exit();
    }

    // finally, render 404 page
    echo $twig->render('404.html', array('rt' => $rt));
    exit();
}

echo $twig->render($rt->get_template(), array('rt' => $rt));
